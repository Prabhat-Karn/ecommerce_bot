"""ecommerceSite URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from Bike_Store import views

from django.contrib.auth import views as auth_views
from django.contrib.auth.decorators import login_required

urlpatterns = [
    path('admin/', admin.site.urls),
    path('category/',views.CategoryList.as_view()),
    path('sub_category/',views.Sub_CategoryList.as_view()),
    path('product/',views.ProductList.as_view()),
    # path('customer/',views.CustomerList.as_view()),
    path('cart/',views.CartList.as_view()),
    
    path('order/',views.OrderList.as_view()),
    path('',views.Index.as_view()),
    # path('clothing/',views.Cloth.as_view()),
    # path('base/',views.Base.as_view()),
    path('home/',views.Home.as_view()),
    path('signup/',views.signup, name = 'signup'),
    path('<str:catName>/<int:prodId>/',views.Order.as_view()),

    # path('cart_detail/',login_required(views.CartDetail.as_view()), name = 'cart_detail'),
    # path('login/', auth_views.LoginView.as_view(template_name = "login.html"), name = 'login'),
    path('login/',views.Login.as_view()),
    # path('accounts/login/',views.login_view),
    # path('logout/',auth_views.LogoutView.as_view(), name = 'logout'),
    path ('logout/', views.Logout.as_view()),
    path('oauth/',include('social_django.urls', namespace = 'social')),
    path('cart_detail/',views.cart_detail, name = 'cart_detail'),

    path('cart_list/',views.ViewCart.as_view()),

    # path('confirm_order/',views.ConfirmOrder.as_view(), name = 'confirm_order'),
   
    path('<str:catName>/',views.Bike.as_view()),
]


