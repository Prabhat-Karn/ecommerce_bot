from django.shortcuts import render, redirect
from rest_framework.views import APIView
from rest_framework.response import Response
import requests;
import json
from rest_framework.status import (
    HTTP_400_BAD_REQUEST,
    HTTP_404_NOT_FOUND,
    HTTP_200_OK
)
# from django.core.serializers import serialize
from django.core import serializers
from django.http import HttpResponse, HttpResponseRedirect
#importing loading from django template  
from django.template import loader
from django.views import View
from .models import Category, Sub_Category, Product, Cart, Order, User
import itertools
# Create your views here.

from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.views.generic import TemplateView

from django.contrib.auth import authenticate, login, logout

from social_django.models import UserSocialAuth

from django.views.decorators.csrf import csrf_exempt, csrf_protect, requires_csrf_token

from Bike_Store.forms import SignUpForm
from  rest_framework.decorators import api_view
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.authtoken.models import Token

def signup(request):

    if request.method  == 'POST':
        form = SignUpForm(request.POST)

        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password = raw_password)
            login(request, user)
            return redirect('home')

    else:
        form = SignUpForm()
    
    return render(request, 'signup.html',{'form':form})

class Login(APIView):
    permission_classes = [AllowAny]
    def post(self, request, fomrat = None):
        username = request.data.get('username')
        password = request.data.get('password')
        if username is None or password is None:
            return Response({'error': 'Please provide both username and password'},
                            status = HTTP_400_BAD_REQUEST)
        user = authenticate(username = username, password = password)
        if not user:
            return Response({'error':'Invalid Credentials'},
                            status = HTTP_404_NOT_FOUND)
        token, _ = Token.objects.get_or_create(user = user)
        return Response({'token':token.key}, status = HTTP_200_OK)

    def get(self, request,format = None):
        return render(request, 'login.html')

class Logout(APIView):
    permission_classes = (IsAuthenticated,)
    def get (self, request, format = None):
        print('.........{}...........'.format(request.user))
        request.user.auth_token.delete()
        return Response (status = HTTP_200_OK)
# def login_view(request):
#     next = request.GET.get('next')
#     form = LoginForm(request.POST or None)
#     if form.is_valid():
#         username = form.cleaned_data.get('username')
#         password = form.cleaned_data.get('password')
#         user = authenticate(username = username, password =password)
#         login(request, user)
#         if next:
#             return redirect(next)
#         return redirect('/')
#     context = {
#         'form':form,
#     }

#     return render(request,'login.html', context)
class CategoryList(APIView):
    def get(self, request, format = None):
        # category = serializers.serialize('json', Category.objects.all())
        
        # cat_queryset = Category.objects.select_related().a
        cat =  Category.objects.all()
        cat_sub = Sub_Category.objects.all()
        # cat1 = []
        # cat2 = []
        final_cat =[]
        for i in cat:
            sub_cat =[]
            for j in cat_sub:

                if i.id == j.category_id:
                   sub_cat.append({"id":j.id,"sub_category":j.sub_category})
            final_cat.append({
                "sub_category":sub_cat,
                "id":i.id,
                "category_name":i.category_name
                            })
        
        if not cat:
            return Response({'error':True, 'Message': 'Category does not exist'}, status=HTTP_400_BAD_REQUEST)
        return Response({'error':False, 'CategoryList': final_cat}, status = HTTP_200_OK)

class Sub_CategoryList(APIView):
    def get(self, request, format = None):
        sub_queryset = Sub_Category.objects.select_related('category').all()
        sub_cat = []
        for i in sub_queryset:
            print(i)
            sub_cat.append({'id': i.id, 'category_id,': i.category.id, 'sub_category': i.sub_category, 'category_name': i.category.category_name})
        if not sub_cat:
            return Response ({'error':True, 'Message': 'Sub Category does not exist'}, status=HTTP_400_BAD_REQUEST)
        return Response({'error':False, 'Sub_CategoryList':sub_cat}, status = HTTP_200_OK)

class ProductList(APIView):
    def get(self, request, format = None):
        prod_queryset = Product.objects.select_related('category','sub_category')
        prod = []
        for i in prod_queryset:
            # prod.append({'id':i.id,'name':i.name,'description':i.description, 'price':i.price, 'quantity':i.quantity, 'brand':i.brand, 'image':str(i.image),'category_name':i.category.category_name,'sub_category':i.sub_category.sub_category})
            if i.sub_category is not None:
                prod.append({'id':i.id,'name':i.name,'description':i.description, 'price':i.price, 'quantity':i.quantity, 'brand':i.brand, 'image':str(i.image),'category_name':i.category.category_name,'sub_category':i.sub_category.sub_category})
                # prod.append({'category_name':i.category.category_name,'sub_category':[{'sub_category':i.sub_category.sub_category,'ProductList':[{'id':i.id,'name':i.name,'description':i.description, 'price':i.price, 'quantity':i.quantity, 'brand':i.brand, 'image':str(i.image)}]}]})
            else:
                prod.append({'id':i.id,'name':i.name,'description':i.description, 'price':i.price, 'quantity':i.quantity, 'brand':i.brand, 'image':str(i.image),'category_name':i.category.category_name,'sub_category':''})
                # print(i.sub_category.sub_category)
                
        if not prod:
            return Response({'error':True, 'Message': 'Product does not exist'}, status=HTTP_400_BAD_REQUEST)
        return Response({'error':False, 'ProductList':prod}, status = HTTP_200_OK)

# class ProductList(APIView):
#     def post(self, request, format = None):
#         category_id = request.data.get('category_id')
#         if category_id:
#             product = Product.objects.filter(category_id = category_id).values('category_id','name', 'description','price','quantity','brand','image')
#         else:
#             return Response({'error':True, 'Message': 'CategoryId is required'}, status=HTTP_400_BAD_REQUEST)
#         if product.count() <= 0:
#             return Response({'error':True, 'Message': 'Product does not exist'}, status=HTTP_400_BAD_REQUEST)
#         return Response({'error':False, 'ProductList':product}, status = HTTP_200_OK)

# class CustomerList(APIView):
#     def get(self, request, format = None):
#         customer = Customer.objects.values('name','address','contact', 'email')
#         if customer.count() <= 0:
#             return Response({'error':True, 'Message': 'Customer does not exist'}, status=HTTP_400_BAD_REQUEST)
#         return Response({'error':False, 'CustomerList':customer}, status = HTTP_200_OK)

class CartList(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request, format = None):
        cart_queryset = Cart.objects.select_related('customer','product')
        cart_list = []
        customer_id = request.user.id
        # cart_list = Cart.objects.filter(customer_id = customer_id).values
        for i in cart_queryset:
            cart_list.append({'id':i.id, 'quantity':i.quantity, 'unit_price':i.unit_price, 'total_price':i.total_price, 'customer_id':customer_id, 'customer_name':request.user.username, 'product_name':i.product.name})
        
        if not cart_list:
            return Response({'error':True, 'Message': 'Cart is Empty'}, status=HTTP_400_BAD_REQUEST)
        return Response({'error':False, 'cart_list':cart_list}, status = HTTP_200_OK)
    # def get(self, request, fomrat = None):
    #     customer_id = request.data.get('customer_id')
    #     product_id = request.data.get('product_id')
    #     if customer_id and product_id:
    #         cart = Cart.objects.filter(customer_id_id = customer_id, product_id_id = product_id).values('quantity','unit_price','total_price')
    #     else:
    #         return Response({'error':True, 'Message': 'customer id and product id is required'}, status=HTTP_400_BAD_REQUEST)
    #     if cart.count() <= 0:
    #         return Response({'error':True, 'Message': 'Product does not exist in cart'}, status=HTTP_400_BAD_REQUEST)
    #     return Response({'error':False, 'CartList':cart}, status = HTTP_200_OK)

@api_view(['POST'])
# @login_required
def cart_detail(request):
    # print(user)
    # username = request.POST.get('username')
    # password = request.POST.get('password')
    # print(username)
    # user = authenticate(request, username=username, password=password)
    # print(user)
    # if user is not None:
    #     login(request,user)
    #     print('---------')
    #     print(request.user)
    #     print('=========')
    print(request.user.username)
    if request.user.is_authenticated:
        abc = Product.objects.filter(id = request.data.get('prod_id')).values('price')
        # abc = Product.objects.filter(id = g['prod_id']).values('price')
        cart_detail = Cart(
            quantity = request.data.get('quantity'),
            product_id = request.data.get('prod_id'),
            unit_price = abc[0]['price'],
            total_price = int(abc[0]['price'])*int(request.data.get('quantity')),
            customer_id = request.user.id,
        )

        cart_detail.save()

        return Response({'authenticated':True, 'Message':'Saved'}, status=HTTP_200_OK)
    return Response({'authenticated':False, 'Message':'User not authenticated'})

    # else:
    #     return Response({'authenticated':False, 'Message':'Please Login'})
# @method_decorator(login_required, name='dispatch')

# class CartDetail(APIView):

    
#     def post(self, request, fomrat = None):

#         print(request.body)
#         print(request.POST['quantity'])
#         abc = Product.objects.filter(id = request.POST['prod_id']).values('price')
#         print(abc[0]['price'])
#         print(self.request.user)
#         cart_detail = Cart(
#             quantity = request.POST['quantity'],
#             product_id = request.POST['prod_id'],
#             unit_price = abc[0]['price'],
#             total_price = abc[0]['price'] * request.POST['quantity'],
#             customer_id = request.user.id,
#         )

#         cart_detail.save()
#         return render(request, 'confirmOrder.html',{'cart_detail':cart_detail})

        # return Response({'error':False,'purchase details':cart_detail}, status = HTTP_200_OK)


class OrderList(APIView):
    def get(self, request, format = None):
        cart_id = request.data.get('cart_id')
        if cart_id:
            order = Order.objects.filter(cart_id_id = cart_id).values('product_total','delivery_charge','grand_total')
        else:
            return Response({'error':True, 'Message': 'cart id is required'}, status=HTTP_400_BAD_REQUEST)
        if order.count() <= 0:
            return Response({'error':True, 'Message': 'Order does not exist in cart'}, status=HTTP_400_BAD_REQUEST)
        return Response({'error':False, 'OrderList':order}, status = HTTP_200_OK)

class Index(View):
    def get(self, request):
        return render(request, 'index.html')
        # return render(request, 'index.html')
        # template = loader.get_template('index.html') # getting our template
        # return HttpResponse(template.render())       # rendering the template in HttpResponse 

class Bike(View):
    def get(self, request, catName):
        return render(request, 'bike.html')
        # r =requests.get('http://127.0.0.1:8000/product/')
        # if()

        # return render(request,'test.html')


class Base(View):
    def get(self, request):
        return render(request, 'base.html')

class Home(View):
    def get(self, request):
        return render(request, 'home.html')

class Order(View):
    def get(self, request, catName, prodId):
        return render(request, 'order.html')

class ViewCart(View):
    def get(self, request):
        return render(request, 'view_cart.html')

# @method_decorator(login_required, name='dispatch')
# class  ConfirmOrder(TemplateView):
#     template_name = 'confirmOrder.html'