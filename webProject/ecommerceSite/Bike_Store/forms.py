from django import forms
from django.contrib.auth.forms import ReadOnlyPasswordHashField
from .models import User
from django.contrib.auth.forms import UserCreationForm
# from django.contrib.auth import authenticate

class UserAdminCreationForm(forms.ModelForm):
    password1 = forms.CharField(label = 'password', widget = forms.PasswordInput)
    password2 = forms.CharField(label = 'password confiirmation', widget = forms.PasswordInput)

    class Meta:
        model = User
        fields = ('username', 'email') 

    def clean_password2(self):
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')
        
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Passwords didn't match")
        return password2

    def save(self, commit = True):
        user = super(UserAdminCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user

class UserAdminChangeForm(forms.ModelForm):
    
    password = ReadOnlyPasswordHashField()

    class Meta:
        model = User
        fields = ('username', 'password', 'active', 'admin')
    
    def clean_password(self):
        return self.initial['password']

# class LoginForm(forms.Form):
#     username = forms.CharField()
#     password = forms.CharField(widget=forms.PasswordInput)
#     def clean(self,*args,**kwargs):
#         username = self.cleaned_data.get('username')
#         password = self.cleaned_data.get('password')

#         if username and password:
#             user = authenticate(username=username, password=password)
#             if not user:
#                 raise forms.ValidationError('User does not exist')
#             if not user.check_password(password):
#                 raise forms.ValidationError('Password Incorrect')
#             if not user.is_active:
#                 raise forms.ValidationError('This user does not exist')
#         return super(LoginForm, self).clean(*args, **kwargs)


class SignUpForm(UserCreationForm):
    first_name = forms.CharField(max_length = 35, required = False, help_text = 'Optional.')
    last_name = forms.CharField(max_length = 35, required = False, help_text = 'Optional.')
    email = forms.EmailField(max_length = 254, help_text = 'Required. Inform a valid email address.')

    class Meta:
        model = User
        fields = ('username','first_name', 'last_name', 'email', 'password1','password2',)

