from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from .forms import UserAdminCreationForm, UserAdminChangeForm
from Bike_Store.models import Category, Sub_Category, Product, Cart, Order


# Register your models here.

User = get_user_model()

class UserAdmin(BaseUserAdmin):
    form = UserAdminChangeForm
    add_form = UserAdminCreationForm

    list_display = ('id','username','admin','staff','active')
    list_filter = ('admin', 'staff', 'active')
    fieldsets = (
        (None, {'fields': ('username', 'password')}),
        ('Personal info', {'fields': ('first_name','last_name','address','contact_no')}),
        ('Permissions', {'fields': ('admin','staff','active')}),
    )

    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('username', 'email','password1', 'password2')}
        ),
    )

    search_fields = ('username',)
    ordering = ('username',)
    filter_horizontal = ()

    def has_add_permission(self, request, obj=None):
        if request.user.is_admin:
            return True
        return False

    def has_delete_permission(self, request, obj=None):
        if request.user.is_admin:
            return True
        return False


    def has_change_permission(self, request, obj=None):
        if request.user.is_admin:
            return True
        return False

class CategoryAdmin(admin.ModelAdmin):
    list_display = ('id','category_name')

class Sub_CategoryAdmin(admin.ModelAdmin):
    list_display = ('id','category_id','sub_category')
    raw_id_fields = ('category',)
    search_fields = ('category_id',)

class ProductAdmin(admin.ModelAdmin):
    list_display = ('id','name','description','price','quantity','brand','image','category_id','sub_category_id')
    raw_id_fields = ('category','sub_category')

class CartAdmin(admin.ModelAdmin):
    list_display = ('id','quantity','unit_price','total_price','customer_id','product_id')

admin.site.register(User, UserAdmin)
admin.site.register(Category, CategoryAdmin)
admin.site.register(Sub_Category, Sub_CategoryAdmin)
admin.site.register(Product, ProductAdmin)
# admin.site.register(Customer)
admin.site.register(Cart, CartAdmin)
admin.site.register(Order)

admin.site.unregister(Group)