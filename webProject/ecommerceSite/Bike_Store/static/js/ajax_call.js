
function openNav() {
    document.getElementById("Sidenav").style.width = "250px";
}
  
function closeNav() {
    document.getElementById("Sidenav").style.width = "0";
}

$(document).ready(function () {
    $.ajax({
        url: "/category/",
        type: "GET",
        success: function (result) {
            console.log(result);

            var i;
            var j = 0;
            var n;
            var cat = document.getElementsByClassName('vertical-menu')[0];
        
            var anch = document.createElement("A");
            anch.setAttribute("href", "javascript:void(0)");
            anch.setAttribute("class", "closebtn");
            anch.setAttribute("onclick", "closeNav()");
            anch.innerHTML = "&times;";
            document.getElementsByClassName('vertical-menu')[0].appendChild(anch);

            var par = document.createElement("P");
            par.innerHTML = "Search by category";
            document.getElementsByClassName('vertical-menu')[0].appendChild(par);

            console.log(result.CategoryList[0].sub_category)
            for (i = 0; i < result.CategoryList.length; i++){
                if (result.CategoryList[i].sub_category == 0 ){
                    cat.innerHTML += `"<a href = '/${result.CategoryList[i].category_name.toLowerCase()}' class = 'active1'></a>"`;
                    cat.getElementsByClassName('active1')[j].innerHTML = result.CategoryList[i].category_name;
                    j++
                }
                else{
                    cat.innerHTML += `<button class = 'dropdown-btn'>${result.CategoryList[i].category_name} <i class="fa fa-caret-down"></i></button>`;
                    cat.innerHTML += `<div class = "dropdown-container"></div>`
                    for (n = 0; n < result.CategoryList[i].sub_category.length; n++){
                        var abc = document.getElementsByClassName('dropdown-container').length-1
                        var catAnc = document.createElement("A")
                        catAnc.setAttribute("href",`/${result.CategoryList[i].sub_category[n].sub_category.toLowerCase()}`)
                        catAnc.innerHTML = `${result.CategoryList[i].sub_category[n].sub_category}`
                        document.getElementsByClassName('dropdown-container')[abc].appendChild(catAnc)
                    }
                    
                }
            }
            var dropdown = document.getElementsByClassName("dropdown-btn");
            var k;
            for (k = 0; k< dropdown.length; k++){
                dropdown[k].addEventListener("click", function(){
                    this.classList.toggle(200)
                    var dropdownContent = this.nextElementSibling;
                    if (dropdownContent.style.display === "block"){
                        dropdownContent.style.display = "none"
                    }
                    else{
                        dropdownContent.style.display = "block"
                    }
                })
            }

        },
        error: function(error) {
            console.log(error);
        }
    })
})


// $(document).ready(function () {
//     $.ajax({
//         url: "http://127.0.0.1:8000/product/",
//         type: "POST",
//         cache: false,
//         dataType: "json",
//         contentType: "application/json",
//         data: JSON.stringify({'category_id': 4}),
//         success: function (result) {
//             console.log(result);
//             var i = 0;
//             for (i; i < result.ProductList.length; i++){

//                 var full_path = result.ProductList[i].image;
//                 var ommited_path = full_path.replace("Bike_Store","");

//                 var d = document.createElement("DIV");
//                 // d.setAttribute("id", "image");
//                 d.setAttribute("class", "column");
//                 document.getElementsByClassName("productLst")[0].appendChild(d);

//                 var x = document.createElement("IMG");
//                 // x.setAttribute("src", `${result.ProductList[0].image}`);
//                 x.setAttribute("src", `${ommited_path}`);
//                 x.setAttribute("width", "304");
//                 x.setAttribute("height", "200");
//                 x.setAttribute("alt", "Gandalf");
//                 document.getElementsByClassName("column")[i].appendChild(x);

//                 // var y = document.createElement("FIGCAPTION")
//                 // y.setAttribute("class", "label");
//                 // document.getElementsByClassName("column")[0].appendChild(y);

//                 var y = document.createElement("P");
//                 y.setAttribute("class", "label");
//                 document.getElementsByClassName("column")[i].appendChild(y);
            
//                 var pro = document.getElementsByClassName('label')[i];
//                 pro.innerHTML = result.ProductList[i].name;
//                 console.log(i);
            
//             }
//         },
//         error: function(error){
//             console.log(error);
//         }
//     })
// })