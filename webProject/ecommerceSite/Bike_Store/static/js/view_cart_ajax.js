$(document).ready(function () {
    $.ajax({
        url: "/cart/",
        type: "GET",
        headers: {'Authorization': `Token ${localStorage.getItem('token')}`,
                    'Content-Type': 'application/json',
                },
        success: function (result) {
            console.log(result)
            var flex_cont = document.createElement('DIV')
            flex_cont.setAttribute('class','flex_cont')
            document.body.appendChild(flex_cont)

            var i;

            

            for (i = 0; i < result.cart_list.length; i++){
                var flex_div = document.createElement('DIV')
                flex_div.setAttribute('class','flex_div')
                document.getElementsByClassName('flex_cont')[0].appendChild(flex_div)

                var name_par = document.createElement('P')
                name_par.setAttribute('class','prod_name')

                var quantity_par = document.createElement('P')
                quantity_par.setAttribute('class','quantity')

                var unit_price_par = document.createElement('P')
                unit_price_par.setAttribute('class','unit_price')

                var total_price_par = document.createElement('P')
                total_price_par.setAttribute('class','total_price')

                document.getElementsByClassName('flex_div')[i].appendChild(name_par)
                document.getElementsByClassName('flex_div')[i].appendChild(quantity_par)
                document.getElementsByClassName('flex_div')[i].appendChild(unit_price_par)
                document.getElementsByClassName('flex_div')[i].appendChild(total_price_par)

                document.getElementsByClassName('prod_name')[i].innerHTML = 'Product Name : ' + result.cart_list[i].product_name
                document.getElementsByClassName('quantity')[i].innerHTML =  'Quantity : ' + result.cart_list[i].quantity
                document.getElementsByClassName('unit_price')[i].innerHTML = 'Unit Price : ' + result.cart_list[i].unit_price
                document.getElementsByClassName('total_price')[i].innerHTML = 'Total Price : ' + result.cart_list[i].total_price
                
               
            }

            var btn_div = document.createElement('DIV')
            btn_div.setAttribute('class','btn_div')

            var order_btn = document.createElement("BUTTON")
            order_btn.setAttribute("type","Submit")
            order_btn.setAttribute('class','order_btn')
            order_btn.innerHTML = "Confirm Order"
            
            document.getElementsByClassName('flex_cont')[0].appendChild(btn_div)
            document.getElementsByClassName('btn_div')[0].appendChild(order_btn)
            
            $(function(){
                $('.order_btn').click(function(e){
                    e.preventDefault()

                    $.ajax({
                        url: "/order/",
                        type: "GET",
                        headers: {'Authorization': `Token ${localStorage.getItem('token')}`,
                                    'Content-Type': 'application/json',
                                },
                        success: function (result) {
                            console.log(result)

                        }
                    })
                })
            })
        },

        statusCode: {
            401: function(){
                window.location = '/login/'
            }
        },
    })
})