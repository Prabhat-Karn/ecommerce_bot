from django.apps import AppConfig


class BikeStoreConfig(AppConfig):
    name = 'Bike_Store'
