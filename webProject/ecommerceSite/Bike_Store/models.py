from django.db import models
from django.core.validators import RegexValidator
from django.contrib.auth.models import(
    AbstractBaseUser, BaseUserManager
)
from social_django import models as oauth_models
# Create your models here.
class UserManager(BaseUserManager):
    def create_user(self, username,email, password = None, is_active = True, is_staff = False, is_admin = False):
        if not username:
            raise ValueError('users most provide username')
        # if not password:
        #     raise ValueError('Password is required')
        user_obj = self.model(username = username)
        user_obj.email =email
        user_obj.set_password(password)
        user_obj.active = is_active
        user_obj.staff = is_staff
        user_obj.admin = is_admin
        user_obj.save(using = self._db)
        return user_obj
    
    def create_staffuser(self, username, password = None):
        user = self.create_user(username, password = password, is_staff = True)
        return user

    def create_superuser(self, username, password = None):
        user = self.create_user(username, password = password, is_staff = True, is_admin = True)
        return user

class User(AbstractBaseUser):
    username = models.CharField(max_length=50, unique = True)
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    email = models.EmailField(max_length=254, unique = True)
    contact_no = models.CharField(max_length=10, validators=[RegexValidator(r'^\d{1,10}$')])
    address = models.CharField(max_length=50)

    active = models.BooleanField(default = True)
    staff = models.BooleanField(default = False)
    admin = models.BooleanField(default = False)

    USERNAME_FIELD = 'username'
    REQUIRED_FIIELDS = []

    objects = UserManager()

    def has_perm(self, perm, obj = None):
        return True

    def has_module_perms(self, app_label):
        return True

    @property
    def is_staff(self):
        return self.staff

    @property
    def is_admin(self):
        return self.admin

    @property
    def is_active(self):
        return self.active

class Category(models.Model):
    category_name = models.CharField(max_length=50)

class Sub_Category(models.Model):
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    sub_category = models.CharField(max_length=100)

class Product(models.Model):
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    sub_category = models.ForeignKey(Sub_Category, on_delete=models.CASCADE, blank = True, null = True)
    name = models.CharField(max_length=200)
    description = models.TextField()
    price = models.IntegerField()
    quantity = models.IntegerField()
    brand = models.CharField(max_length=50)
    image = models.ImageField(upload_to = './Bike_Store/static/Images/', height_field=None, width_field=None, max_length=None)
    # image = models.FileField(upload_to='Bike_Store/static/Images/')

# class Customer(models.Model):
#     name = models.CharField(max_length=50)
#     address = models.CharField(max_length=50)
#     contact = models.CharField(unique=True, max_length=10, validators=[RegexValidator(r'^\d{1,10}$')])
#     email = models.EmailField(max_length=254)

class Cart(models.Model):
    # customer = models.ForeignKey(oauth_models.USER_MODEL, on_delete=models.CASCADE)
    customer = models.ForeignKey(User, on_delete=models.CASCADE)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    quantity = models.IntegerField()
    unit_price = models.IntegerField()
    total_price = models.BigIntegerField()

class Order(models.Model):
    cart = models.ForeignKey(Cart, on_delete=models.CASCADE)
    product_total = models.IntegerField()
    delivery_charge = models.IntegerField()
    grand_total = models.IntegerField()
