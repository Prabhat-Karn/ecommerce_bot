# This files contains your custom actions which can be used to run
# custom Python code.
#
# See this guide on how to implement these action:
# https://rasa.com/docs/rasa/core/actions/#custom-actions/


# This is a simple example for a custom action which utters "Hello World!"

from typing import Any, Text, Dict, List

from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.events import SlotSet, AllSlotsReset
import requests
import json
import webbrowser
#
#
# class ActionHelloWorld(Action):
#
#     def name(self) -> Text:
#         return "action_hello_world"
#
#     def run(self, dispatcher: CollectingDispatcher,
#             tracker: Tracker,
#             domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
#
#         dispatcher.utter_message(text="Hello World!")
#
#         return []


# class ActionEntitySearch(Action):

#     def name(self) -> Text:
#         return "action_entity_search"

#     def run(self, dispatcher: CollectingDispatcher,
#             tracker: Tracker,
#             domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
#         entity_ = tracker.get_slot("Entity")
#         mountain_url = 'https://epicmountainbike.com/product-tag/ktm/'
#         road_url = 'https://www.jensonusa.com/Road-Bikes'
#         touring_url = 'https://www.bikester.co.uk/bikes/touring-bikes.html'

#         # res = requests.get(mountain_url)
#         bike_type = tracker.get_slot("Detail")
#         bike_type_ = bike_type.lower()
#         if bike_type_ == "mountain":
#             dispatcher.utter_message("visit the link to get the {} {}: {}".format(bike_type, entity_, mountain_url))
#         if bike_type_ == "road":
#             dispatcher.utter_message("visit the link to get the {} {}: {}".format(bike_type, entity_, road_url))
#         if bike_type_ == "touring":
#             dispatcher.utter_message("visit the link to get the {} {}: {}".format(bike_type, entity_, touring_url))
#         return [SlotSet("Detail", bike_type_)]

class ActionShowCategories(Action):
    def name(self):
        return "action_show_categories"

    def run (self, dispatcher, tracker, domain):
        url = "https://localhost:8000/category/"
        res = requests.get(url, verify=False)
        cat = res.json()['CategoryList']

        # cat_name = []
        # for i in cat:
        #     cat_name.append(i['category_name'])
        response = "Here is the top categories:\n{}".format([i['category_name'] for i in cat])
        dispatcher.utter_message(response)

class ActionSubCategories(Action):
    def name(self):
        return "action_sub_categories"

    def run(self, dispatcher, tracker, domain):


        entity_ = tracker.get_slot('Entity')
        # entity_lower = entity_.lower()

        url = "https://localhost:8000/category/"
        res = requests.get(url, verify = False)
        # cat = res.json()['CategoryList'][0]['sub_category'][0]['sub_category']
        cat = res.json()['CategoryList']
        sub_cat = []
        for i in cat:
            if i['category_name'] == 'Bike':
                for j in i['sub_category']:
                    sub_cat.append(j['sub_category'])
            

                response = "We have following type of bikes:\n{}".format(sub_cat)
        dispatcher.utter_message(response)

        return [SlotSet("Entity", entity_)]

class ActionProducts(Action):
    def name(self):
        return "action_products"

    def run (self, dispatcher, tracker, domain):
        entity_ = tracker.get_slot('Entity')
        detail_ = tracker.get_slot('Detail')
        detail_lower =None
        entity_lower = entity_.lower()

        url = "https://localhost:8000/product/"
        res = requests.get(url, verify = False)
        prod = res.json()['ProductList']

        prod_name = []
        # if detail_== None:
        #     dispatcher.utter_message(text = "here are products: ", json_message= {"payload":"cardsCarousel", "data":prod})
        for i in prod:
            split_cat = i['category_name'].split()
            split_sub_cat = i['sub_category'].split()

            # prod_name.append(i['sub_category'])
            if detail_ == None:
                for k in range(len(split_cat)):
                    if split_cat[k].lower() == entity_lower:
                        prod_name.append({'name':i['name'], 'image':i['image'], 'id':i['id'], 'cat_name':i['category_name']})
                        dispatcher.utter_message(text = "here are the {}: ".format(i['category_name']), json_message= {"payload":"cardsCarousel", "data":prod_name})
            else:
                detail_lower = detail_.lower()
                for j in range(len(split_sub_cat)):
                    if split_sub_cat[j].lower() == detail_lower:
                        prod_name.append({'name': i['name'], 'image': i['image'], 'id':i['id'], 'cat_name':i['sub_category']})
                        dispatcher.utter_message(text = "here are the {}".format(i['sub_category']), json_message= {"payload":"cardsCarousel", "data":prod_name})
        
       

        return [SlotSet("Entity", entity_lower), SlotSet("Detail",detail_lower)]

class ActionCartList(Action):
    def name(self):
        return 'action_cart_list'

    def run(self,dispatcher,tracker,domain):
        url = "https://localhost:8000/cart/"
        res = requests.get(url, headers = {'Authorization':'Token {}'.format(tracker.sender_id)}, verify=False)
        # res = requests.get(url, verify=False)
        if res.status_code == 401:
            webbrowser.open('https://localhost:8000/login')
            dispatcher.utter_message("Please Login")
        
        else:
            cart = res.json()['cart_list']

            response = "Here is the cartlist:\n{}".format([i['product_name'] for i in cart])
            dispatcher.utter_message(response)

        






class ActionSlotReset(Action):
    def name(self):
        return 'action_slot_reset'
    
    def run(self, dispatcher, tracker, domain):
        return[AllSlotsReset()]
