## happy path
* greet
  - utter_greet
* mood_great
  - utter_happy

## sad path 1
* greet
  - utter_greet
* mood_unhappy
  - utter_cheer_up
  - utter_did_that_help
* affirm
  - utter_happy

## sad path 2
* greet
  - utter_greet
* mood_unhappy
  - utter_cheer_up
  - utter_did_that_help
* deny
  - utter_goodbye

## say goodbye
* goodbye
  - utter_goodbye

## bot challenge
* bot_challenge
  - utter_iamabot
  

## interactive_story_1
* ShowTopCategories
    - action_show_categories

## interactive_story_1
* Explore{"Entity": "bikes"}
    - slot{"Entity": "bikes"}
    - action_sub_categories
    - slot{"Entity": "bikes"}


## interactive_story_1
* ShowTopCategories
    - action_show_categories
* Explore{"Entity": "bikes"}
    - slot{"Entity": "bikes"}
    - action_sub_categories
    - slot{"Entity": "bikes"}
* Explore{"Detail": "mountain", "Entity": "bikes"}
    - slot{"Detail": "mountain"}
    - slot{"Entity": "bikes"}
    - action_products
    - slot{"Entity": "bikes"}
    - slot{"Detail": "mountain"}
    - action_slot_reset
    - reset_slots
* Explore{"Entity": "clothing"}
    - slot{"Entity": "clothing"}
    - action_products
    - slot{"Entity": "clothing"}
    - slot{"Detail": null}
    - action_slot_reset
    - reset_slots

## interactive_story_1
* Explore{"Entity": "clothing"}
    - slot{"Entity": "clothing"}
    - action_products
    - slot{"Entity": "clothing"}
    - slot{"Detail": null}

## interactive_story_1
* Explore{"Entity": "clothing"}
    - slot{"Entity": "clothing"}
    - action_products
    - slot{"Entity": "clothing"}
    - slot{"Detail": null}
    - action_slot_reset
    - reset_slots

## interactive_story_1
* ShowCart
    - action_cart_list
